def trojkat(n):
    for i in range(0, n+1):
        print(' ' * (n-i) + '+' * (2*i - 1))


# trojkat(4)


def trojkat2(x, y):
    for i in range(1, x + 1):
        print(y*('+' * i + ' ' * (x - i) + ' '))


# trojkat2(5, 3)


def romb(n):
    for i in range(1, n+1):
        print(' ' * (n-i) + '+' * (2*i - 1))
    for j in range(1, n):
        print(' ' * j + '+' * (2*(n-j)-1))


# romb(3)


# alphabet = [chr(x) for x in range(97, 123)]
#
# points = [x for x in range(1, 27)]
#
# a = list(zip(alphabet, points))
#
# di = {k: v for k, v in zip(alphabet, points)}


# FUNKCJA CHOINKA (wysokosc choinki, ilosc segmentow) - ogon 10% wysokosci

def choinka(h, n):
    # gwiazdka :)
    print(' ' * (h-1) + '*')

    # choinka
    for i in range(1, n+1):
        for j in range(1, h+1):
            print(' ' * (h-j) + '+' * (2*j - 1))

    # wysokosc nogi - warunek (wysokosc min. 1)
    h_nogi = int(h * n * 0.1)
    if h_nogi < 1:
        h_nogi = 1

    # wysokosc nogi
    for k in range(0, h_nogi):
        print(' ' * (h-1) + '+')


# choinka(10, 2)

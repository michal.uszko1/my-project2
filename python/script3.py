class Car:
    acc = 10

    def __init__(self, reg_num):
        self.reg_num = reg_num
        self.in_motion = False
        self.speed = 0

    def print_info(self):
        print(f'Acceleration: {self.acc}')
        print(f'Registration no.: {self.reg_num}')
        print(f'Is in motion: {self.in_motion}')
        print(f'Speed : {self.speed}')

    def drive(self):
        self.in_motion = True

    def accelerate(self, acce):
        if self.in_motion:
            self.speed += acce
        else:
            print('Car is not in motion')
            print()

    def stop(self):
        self.in_motion = False
        self.speed = 0


audi = Car(123)
volvo = Car(456)
mercedes = Car(789)


class Employee:

    raise_amount = 1.04

    def __init__(self, first_name, last_name, email, salary):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.salary = salary

    def apply_raise(self):
        self.salary *= self.raise_amount

    def __str__(self):
        return f"First name: {self.first_name}\n" \
               f"Last name: {self.last_name}\n" \
               f"Email: {self.email}\n" \
               f"Salary: {self.salary}"

    def oblicz_podatek_dochodowy(self, prog_dochodowy=85000):
        self.roczna_pensja = self.salary * 12

        if self.roczna_pensja < prog_dochodowy:
            print(f'Twoj podatek dochodowy to {self.roczna_pensja*0.17}')
        else:
            podatek_pierwszy_prog = prog_dochodowy * 0.17
            podatek_drugi_prog = (self.roczna_pensja - prog_dochodowy) * 0.32
            podatek_laczny = podatek_pierwszy_prog + podatek_drugi_prog
            print(f'Twoj podatek dochodowy to {podatek_laczny}')

    @classmethod
    def ustaw_roczna_podwyzke(cls, nowa_roczna_podwyzka):
        cls.raise_amount = nowa_roczna_podwyzka

    @staticmethod
    def wyswietl_info_podatkowe(pierwszy, drugi):
        print(f'Pierwszy próg podatkowy: {pierwszy}')
        print(f'Drugi próg podatkowy: {drugi}')


class Developer(Employee):
    raise_amount = 1.1
    
    def __init__(self, first_name, last_name, email, salary, program_lng):
        super().__init__(first_name, last_name, email, salary)
        self.program_lng = program_lng


e1 = Employee('p', 'Kowalski', 'ab@gmail.com', 10000)
e2 = Developer('p', 'Wisniewska', 'cd@gmail.com', 10000, 'Python')

# e2.raise_amount = 1.1
# e1.apply_raise()
# print(e1.salary)
# e1.oblicz_podatek_dochodowy()
#
# e2.apply_raise()
# print(e2.salary)
e2.oblicz_podatek_dochodowy()

# print(f'Stara roczna podwyzka {Employee.raise_amount}')
# Employee.ustaw_roczna_podwyzke(1.2)
# print(f'Nowa roczna podwyzka {Employee.raise_amount}')

print(e1.raise_amount)
print(e2.program_lng)

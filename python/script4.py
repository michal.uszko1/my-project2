while True:
    user = input()

    if user == 'czesc' or user == 'siema' or user == 'hej':
        print('czesc jak sie masz')

    user = input()

    if user == 'dobrze' or user == 'wspaniale' or user == 'kapitalnie':
        print('Idealnie, jak Ci mogę pomóc? ')
    elif user == 'źle' or user == 'słabo' or user == 'okropnie':
        print('Oj to szkoda, jak Ci mogę poprawić humor?')
        user = input()
        if user == 'kawał' or user == 'coś śmiesznego':
            print('Co robi 9 złotych w portfelu? Ledwo dycha')

    break


dict_conv = {
    ('czesc') : 'czesc',
    ('dobrze') : 'super',
    ('zle') : 'szkoda',
    ('kawal') : 'nie'
}


def conv_with_bot(human_input):
    for key, value in dict_conv.items():
        if human_input.lower() in key:
            return value
        return 'No command'


if __name__ == '__main__':
    while True:
        word = input()
        output = conv_with_bot(word)
        print(output)
        rerun = input('Continue? [y]/n')
        if rerun.lower() == 'y':
            break
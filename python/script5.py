import sys
import platform
import pandas as pd


print(f'{platform.system()} {platform.release()}')
print(pd.__version__)


def deco(func):
    def wrapper(*args, **kwargs):
        print("Hello, what's your name?")
        func(*args, **kwargs)
        print('Nice to meet you')
    return wrapper


def hello(name):
    print(f'Hello, my name is {name}')


@deco
def hello2(name):
    print(f'Hello, my name is {name}')


deco(hello)('Ala')
print()
hello2('Ala')


def is_even(func):
    def wrapper(number):
        func(number)
        if number % 2 == 0:
            print('Number is even')
        else:
            print('Number is odd')
            number += 1
        func(number)
    return wrapper


@is_even
def show_number(number):
    print(f'My number is {number}')


show_number(5)



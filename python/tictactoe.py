import random


def display_board(func):

    def wrapper(board, mark):
        field = func(board, mark)
        print('+--' + '---+')
        print('|' + field[6] + '|' + field[7] + '|' + field[8] + '|')
        print('+--' + '---+')
        print('|' + field[3] + '|' + field[4] + '|' + field[5] + '|')
        print('+--' + '---+')
        print('|' + field[0] + '|' + field[1] + '|' + field[2] + '|')
        print('+--' + '---+')
        return board
    return wrapper


@display_board
def create_empty_board(board, mark):
    for n in range(0, 9):
        board.append(mark)
    return board


@display_board
def player_fill_board(board, mark):
    while True:
        player_input = input('Choose a field to fill (from 1 to 9): ')
        available_signs = ['1', '2', '3', '4', '5', '6', '7', '8', '9']

        if player_input not in available_signs:
            print('You have to chose a number from 1 to 9')
            continue

        player_chosen_field = int(player_input) - 1

        if check_if_field_is_empty(board, player_chosen_field):
            board[player_chosen_field] = mark
            break
        else:
            print('This field is not empty. Please, select another one.')
            continue
    return board


@display_board
def computer_fill_board(board, mark):
    while True:
        computer_input = random.randint(1, 9)
        computer_chosen_field = computer_input - 1
        if check_if_field_is_empty(board, computer_chosen_field):
            board[computer_chosen_field] = mark
            break
        else:
            continue
    return board


def check_if_field_is_empty(board, field):
    return board[field] == ' '


def check_if_board_is_full(board):
    return ' ' not in board


def check_win_condition_rows(board, mark):
    return board[6] == mark and board[7] == mark and board[8] == mark or \
           board[3] == mark and board[4] == mark and board[5] == mark or \
           board[0] == mark and board[1] == mark and board[2] == mark


def check_win_condition_columns(board, mark):
    return board[2] == mark and board[5] == mark and board[8] == mark or \
           board[1] == mark and board[4] == mark and board[7] == mark or \
           board[0] == mark and board[3] == mark and board[6] == mark


def check_win_condition_diagonal(board, mark):
    return board[0] == mark and board[4] == mark and board[8] == mark or \
           board[6] == mark and board[4] == mark and board[2] == mark


def select_first_player():
    return random.randint(0, 1)


while True:
    playboard = []

    create_empty_board(playboard, ' ')

    first_player = select_first_player()

    if first_player == 0:
        print('You start first.')
    else:
        print('Computer starts first.')

    while True:

        if first_player == 0:
            player_fill_board(playboard, 'O')
            if not check_if_board_is_full(playboard):
                computer_fill_board(playboard, 'X')

            if check_win_condition_columns(playboard, 'O') or \
                    check_win_condition_rows(playboard, 'O') or \
                    check_win_condition_diagonal(playboard, 'O'):
                print('Player won!')
                break

            if check_win_condition_columns(playboard, 'X') or \
                    check_win_condition_rows(playboard, 'X') or \
                    check_win_condition_diagonal(playboard, 'X'):
                print('Computer won!')
                break
        else:
            computer_fill_board(playboard, 'O')

            if check_win_condition_columns(playboard, 'O') or \
                    check_win_condition_rows(playboard, 'O') or \
                    check_win_condition_diagonal(playboard, 'O'):
                print('Computer won!')
                break

            if not check_if_board_is_full(playboard):
                player_fill_board(playboard, 'X')

            if check_win_condition_columns(playboard, 'X') or \
                    check_win_condition_rows(playboard, 'X') or \
                    check_win_condition_diagonal(playboard, 'X'):
                print('Player won!')
                break

        if check_if_board_is_full(playboard):
            print('Tie')
            break

    play_again = input('Do you want to play again? [y]/n ')

    if play_again.lower() == 'y':
        continue
    else:
        break
